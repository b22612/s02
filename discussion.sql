-- To Start MariaDB Server: mysql -u root

-- Lists down the databases inside the DBMS
-- not case sensitive but easier to identify queries if all CAPS
SHOW DATABASES;


-- Create a database,
CREATE DATABASE music_db;

-- Remove a specific database
-- Be careful with dropping a database, it proceed with the query without any warning
DROP DATABASE music_db;

-- Select a database
USE music_db;


-- Create a table
-- Table columns have the following format
    -- [column_name] [data_type] [other_options]

    -- id - primary key

        -- INTEGER FORMAT
        -- NOT NULL
        -- AUTO INCREMENT the id upon saving a user; starts with 1
        -- VARCHAR(50) - Variable Character, the storage size of the datatype is equal to the actual lenght of the entered string in bytes. 50 is used to set the character limit.
    CREATE TABLE users(
        id INT NOT NULL AUTO_INCREMENT,
        username VARCHAR(50) NOT NULL,
        password VARCHAR(50) NOT NULL,
        full_name VARCHAR(50) NOT NULL,
        contact_number VARCHAR(50) NOT NULL,
        email VARCHAR(50) NOT NULL,
        address VARCHAR(50) NOT NULL,
        primary KEY (id)
    );

    SHOW TABLES;

    CREATE TABLE artists(
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(50) NOT NULL,
        primary KEY (id)
    );

    -- show the columns and rows of a table
    DESCRIBE <table name>;

    -- When to create a foreign key constraint? 
    -- If the relationship is one-to-many, "foreign key" column is added in the many entity/table.
    -- If the relationship is one-to-one, "primary key" of the parent row is added as "foreign key" of the child row
    -- If a linking/associative table exists, linking table is created for the many-to-many relationship and a foreign key for both tables/entities is added

    -- DATE AND TIME FORMAT IN SQL
    -- Date referes to YYYY-MM-DD
    -- TIME refers to HH:MM:SS
    -- DATETIME refers to YYYY-MM-DD HH:MM:SS

    -- CONSTRAINT - used to specify the rules for the data table, this is an optional field, but we use this one to identify the foreign key
    -- FOREIGN KEY: is used to prevent actions that will destroy links between tables
    -- on delete restrict - restricts delete if there is a foreign key

    CREATE TABLE albums(
        id INT NOT NULL AUTO_INCREMENT,
        album_title VARCHAR(50) NOT NULL,
        date_released DATE NOT NULL,
        artists_id INT NOT NULL,
        PRIMARY KEY(id),
        CONSTRAINT fk_albums_artist_id
            FOREIGN KEY (artists_id) REFERENCES artists(id)
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
    );

    CREATE TABLE songs(
        id INT NOT NULL AUTO_INCREMENT,
        song_name VARCHAR(50) NOT NULL,
        length TIME NOT NULL,
        genre VARCHAR(50) NOT NULL,
        albums_id INT NOT NULL,
        PRIMARY KEY(id),
        CONSTRAINT fk_songs_albums_id
            FOREIGN KEY (albums_id) REFERENCES albums(id)
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
    );
    

    CREATE TABLE playlists (
        id INT NOT NULL AUTO_INCREMENT,
        user_id INT NOT NULL,
        datetime_created DATETIME NOT NULL,
        PRIMARY KEY(id),
        CONSTRAINT fk_playlists_user_id
            FOREIGN KEY(user_id) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
    );

    CREATE TABLE playlists_songs(
        id INT NOT NULL AUTO_INCREMENT,
        playlist_id INT NOT NULL,
        song_id INT NOT NULL,
        PRIMARY KEY (id),
        CONSTRAINT fk_playlists_songs_playlist_id
            FOREIGN KEY (playlist_id) REFERENCES playlists(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT,
        CONSTRAINT fk_playlists_songs_song_id
            FOREIGN KEY (song_id) REFERENCES songs(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
    );