CREATE DATABASE blog_db;

USE blog_db;

    CREATE TABLE users(
        id INT NOT NULL AUTO_INCREMENT,
        email VARCHAR(100) NOT NULL,
        password VARCHAR(300) NOT NULL,
        datetime_created DATETIME NOT NULL,
        primary KEY (id)
    );

    CREATE TABLE posts(
        id INT NOT NULL AUTO_INCREMENT,
        title VARCHAR(500) NOT NULL,
        content VARCHAR(5000) NOT NULL,
        datetime_posted DATETIME NOT NULL,
        users_id INT NOT NULL,
        PRIMARY KEY(id),
        CONSTRAINT fk_posts_user_id
            FOREIGN KEY (users_id) REFERENCES users(id)
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
    );

    CREATE TABLE post_comments(
        id INT NOT NULL AUTO_INCREMENT,
        posts_id INT NOT NULL,
        users_id INT NOT NULL,
        content VARCHAR(5000) NOT NULL,
        datetime_commented DATETIME NOT NULL,
        PRIMARY KEY(id),
        CONSTRAINT fk_posts_comments_posts_id
            FOREIGN KEY (posts_id) REFERENCES posts(id)
            ON UPDATE CASCADE 
            ON DELETE RESTRICT,
        CONSTRAINT fk_posts_comments_user_id
            FOREIGN KEY (users_id) REFERENCES users(id)
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
    );

CREATE TABLE post_likes(
        id INT NOT NULL AUTO_INCREMENT,
        posts_id INT NOT NULL,
        users_id INT NOT NULL,
        datetime_liked DATETIME NOT NULL,
        PRIMARY KEY(id),
        CONSTRAINT fk_posts_likes_posts_id
            FOREIGN KEY (posts_id) REFERENCES posts(id)
            ON UPDATE CASCADE 
            ON DELETE RESTRICT,
        CONSTRAINT fk_posts_likes_user_id
            FOREIGN KEY (users_id) REFERENCES users(id)
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
    );
    